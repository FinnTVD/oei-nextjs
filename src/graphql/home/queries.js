import { gql } from '@apollo/client';

const GET_SECTION_SITEMAP = gql`
    query {
        pages(where: { title: "Home Page" }) {
            edges {
                node {
                    homePage {
                        sectionSitemap {
                            year
                            country
                            description
                            impressiveNumbers
                            backgroundImage {
                                sourceUrl
                            }
                            mapImage {
                                sourceUrl
                            }
                            mapImage {
                                sourceUrl
                            }
                            circleLarge {
                                sourceUrl
                            }
                            circleSmall {
                                sourceUrl
                            }
                            contentCircleLarge {
                                title
                                description {
                                    content
                                }
                                image {
                                    sourceUrl
                                }
                            }
                            contentCircleSmall {
                                title
                                description {
                                    content
                                }
                                image {
                                    sourceUrl
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

export { GET_SECTION_SITEMAP };
