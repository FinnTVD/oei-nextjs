'use client';

import Footer from '@/components/Home/Footer';
import Header from '@/components/Home/Header';
import Member from '@/components/Home/Member';
import News from '@/components/Home/News';
import Partner from '@/components/Home/Partner';
import Service from '@/components/Home/Service';
import SiteMap from '@/components/Home/SiteMap';
import About from '@/components/Home/About';

export default function Home() {
    return (
        <>
            <Header />
            <main>
                <About />
                <Member />
                <Service />
                <SiteMap />
                <Partner />
                <News />
            </main>
            <Footer />
        </>
    );
}
