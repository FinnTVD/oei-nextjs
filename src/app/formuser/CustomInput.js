import { useState } from 'react';

let arrNew = [];

export default function CustomInput({
    input,
    setInputNominee,
    setInputCv,
    setInputContent,
    setInputEmail,
    setInputPhone,
    setInputName,
}) {
    const [srcImg, setSrcImg] = useState('');
    console.log('🚀 ~ file: CustomInput.js:13 ~ srcImg:', srcImg);
    return (
        <div>
            <label htmlFor="">{input.label}</label>
            {input.type?.toLowerCase() === 'select' ? (
                <select
                    defaultValue={'default'}
                    name=""
                    id=""
                    required={input.isRequired}
                    onChange={(e) =>
                        setInputNominee((pre) => ({
                            ...pre,
                            id: input.id,
                            value: e.target.value,
                        }))
                    }
                >
                    <option value="default" disabled>
                        {input.placeholder}
                    </option>
                    {input.choices.map((item, index) => (
                        <option key={index} value={item.value}>
                            {item.text}
                        </option>
                    ))}
                </select>
            ) : input.type?.toLowerCase() === 'textarea' ? (
                <textarea
                    id="content"
                    className="w-full p-4 bg-transparent border border-gray-200 rounded-lg outline-none resize-none min-h-[150px]"
                    placeholder="Enter description"
                    onChange={(e) =>
                        setInputContent((pre) => ({
                            ...pre,
                            id: input.id,
                            value: e.target.value,
                        }))
                    }
                />
            ) : (
                <input
                    onChange={(e) => {
                        if (input.type?.toLowerCase() === 'fileupload') {
                            return setInputCv((pre) => ({
                                ...pre,
                                id: input.id,
                                files: [e.target.files[0]],
                            }));
                        }
                        if (input.type?.toLowerCase() === 'phone') {
                            return setInputPhone((pre) => ({
                                ...pre,
                                id: input.id,
                                value: e.target.value,
                            }));
                        }
                        if (input.type?.toLowerCase() === 'email') {
                            return setInputEmail((pre) => ({
                                ...pre,
                                id: input.id,
                                emailValues: {
                                    value: e.target.value,
                                },
                            }));
                        }
                        return setInputName((pre) => ({
                            ...pre,
                            id: input.id,
                            value: e.target.value,
                        }));
                    }}
                    type={
                        input.type?.toLowerCase() === 'fileupload'
                            ? 'file'
                            : input.type?.toLowerCase() === 'phone'
                            ? 'tel'
                            : input.type?.toLowerCase() === 'email'
                            ? 'email'
                            : input.type?.toLowerCase()
                    }
                    required={input.isRequired}
                />
            )}
            <img src={srcImg} alt="" />
        </div>
    );
}
