'use client';

import { CREATE_FORM, GET_FORM } from '@/graphql/form/queries';
import { useMutation, useQuery } from '@apollo/client';
import CustomInput from './CustomInput';
import { useState } from 'react';

const formId = '1'; // Thay thế bằng ID của biểu mẫu Gravity Forms
const inputValues = {
    // Thay thế bằng các giá trị của các trường trong biểu mẫu
    field1: 'value1',
    field2: 'value2',
    field3: 'value3',
};

export default function FormPage() {
    const [inputNominee, setInputNominee] = useState(null);
    const [inputName, setInputName] = useState(null);
    const [inputPhone, setInputPhone] = useState(null);
    const [inputEmail, setInputEmail] = useState(null);
    const [inputContent, setInputContent] = useState(null);
    const [inputCv, setInputCv] = useState(null);
    console.log('🚀 ~ file: page.js:23 ~ FormPage ~ inputCv:', inputCv);
    const {
        loading: loadingForm,
        error: errorForm,
        data: dataForm,
    } = useQuery(GET_FORM, {
        variables: { id: 1, first: 100 },
    });

    const [
        mutateFunction,
        { data: dataMutate, loading: loadingMutate, error: errorMutate },
    ] = useMutation(CREATE_FORM);
    if (loadingForm) return <div className="text-black">Loading...</div>;
    if (errorForm || errorMutate)
        return (
            <div className="text-black">
                {errorForm?.message || errorMutate?.message}
            </div>
        );
    const { gfForm } = dataForm;

    const handleOnSubmit = (e) => {
        e.preventDefault();
        const input = {
            id: 1,
            fieldValues: [
                inputName,
                inputPhone,
                inputEmail,
                inputNominee,
                inputContent,
                inputCv,
            ],
        };
        console.log('🚀 ~ file: page.js:58 ~ handleOnSubmit ~ input:', input);
        mutateFunction({
            variables: {
                input: input,
            },
        });
    };
    return (
        <div className="text-black h-[100vh] w-full">
            <h4>{gfForm.title}</h4>
            <p>{gfForm.description}</p>
            <form onSubmit={handleOnSubmit} action="">
                {gfForm.formFields.nodes.map((item, index) => (
                    <CustomInput
                        setInputNominee={setInputNominee}
                        setInputCv={setInputCv}
                        setInputContent={setInputContent}
                        setInputEmail={setInputEmail}
                        setInputPhone={setInputPhone}
                        setInputName={setInputName}
                        key={index}
                        input={item}
                    />
                ))}
                <button>{gfForm.button.text}</button>
            </form>
        </div>
    );
}
