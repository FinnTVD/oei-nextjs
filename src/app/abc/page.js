'use client';

import {
    MapContainer,
    GeoJSON,
    Marker,
    Popup,
    Tooltip,
    LayersControl,
    FeatureGroup,
    Circle,
    Rectangle,
} from 'react-leaflet';

import map1 from '../../geoison/THA.json';
import map2 from '../../geoison/HS.json';
import map3 from '../../geoison/TS.json';
import map4 from '../../geoison/VNM.json';
import map5 from '../../geoison/TWN.json';
import map6 from '../../geoison/KHM.json';
import map7 from '../../geoison/LAO.json';
import { useEffect, useRef, useState } from 'react';
import { ResetCenterView } from '@/components/abc/Test';

const customIconSmall = L.icon({
    iconUrl:
        'https://demo1.okhub.tech/wp-content/uploads/2023/05/Group-427322901.png',
    iconSize: [60, 60],
    iconAnchor: [30, 30],
    className: 'small-marker',
});
const customIconLarge = L.icon({
    iconUrl:
        'https://demo1.okhub.tech/wp-content/uploads/2023/05/Vong-tron.png',
    iconSize: [300, 300],
    iconAnchor: [150, 150],
    popupAnchor: [150, 150],
    className: 'large-marker',
});

const rectangle = [
    [16.01811203785642, 107.0054873134701],
    [16.31811203785642, 107.0454873134701],
];

const arrMarker = [
    {
        id: 1,
        lat: 19.814086328570294,
        lng: 105.79300550483987,
    },
    {
        id: 2,
        lat: 20,
        lng: 103,
    },
];

export default function ABCPage() {
    const [selectPosition, setSelectPosition] = useState(null);
    const [isMarkerClicked, setIsMarkerClicked] = useState(false);
    const [idMarker, setIdMarker] = useState(0);
    console.log('🚀 ~ file: page.js:63 ~ ABCPage ~ idMarker:', idMarker);
    const [map, setMap] = useState(null);
    const popupElRef = useRef(null);

    useEffect(() => {
        document.querySelector(
            '.leaflet-control-attribution.leaflet-control'
        ).style.display = 'none';
    }, []);

    const handleMarkerClick = (id) => {
        setIsMarkerClicked(true);
        setIdMarker(id);
    };

    const handlePopupClose = () => {
        if (!popupElRef.current || !map) return;
        setIsMarkerClicked(false);
        setIdMarker(0);
        map.closePopup();
    };

    return (
        <>
            <div className="text-black">
                <div
                    onClick={() =>
                        setSelectPosition({
                            lat: '16.31811203785642',
                            lon: '107.0454873134701',
                        })
                    }
                >
                    vn
                </div>
                <div
                    onClick={() =>
                        setSelectPosition({
                            lat: '13.804684473312848',
                            lon: '100.52674027912768',
                        })
                    }
                >
                    thailand
                </div>
            </div>
            <MapContainer
                center={[16.31811203785642, 107.0454873134701]}
                zoom={6.3}
                scrollWheelZoom={false}
                className="w-[100vw] h-[100vh]"
                ref={setMap}
            >
                {/* <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map1.features}
                /> */}
                <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map2.features}
                />
                <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map3.features}
                />
                <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map4.features}
                />
                {/* <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map5.features}
                /> */}
                {/* <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map6.features}
                />
                <GeoJSON
                    style={{
                        fillColor: '#0D3623',
                        fillOpacity: 1,
                        color: '#0D3623',
                    }}
                    data={map7.features}
                /> */}
                {arrMarker.map((mar) => (
                    <Marker
                        key={mar.id}
                        position={[mar.lat, mar.lng]}
                        icon={
                            isMarkerClicked && mar.id === idMarker
                                ? customIconLarge
                                : customIconSmall
                        }
                        eventHandlers={{
                            click: () => handleMarkerClick(mar.id),
                            popupclose: () => {
                                if (isMarkerClicked)
                                    return setIsMarkerClicked(false);
                            },
                        }}
                    >
                        <Popup
                            ref={popupElRef}
                            closeButton={false}
                            className="relative"
                        >
                            <div className="absolute top-[calc(4.8*100vw/100)] left-[calc(9.75*100vw/100)] w-[calc(38.75*100vw/100)] h-[calc(24.25*100vw/100)]">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/mclsa6fpz1zfeuht1hlx08lgqndupv42.jpg.png"
                                    alt=""
                                />
                                <div className="absolute top-0 left-0 flex items-center justify-center w-full h-full text-white">
                                    <h4>BEN TRE VPL WPF</h4>
                                    <ul>
                                        <li className="flex gap-x-4">
                                            <span className="block w-6 h-6 bg-white rounded-full"></span>{' '}
                                            <span>EPC Contractor : PECC2</span>
                                        </li>
                                        <li className="flex gap-x-4">
                                            <span className="block w-6 h-6 bg-white rounded-full"></span>{' '}
                                            <span>EPC Contractor : PECC2</span>
                                        </li>
                                        <li className="flex gap-x-4">
                                            <span className="block w-6 h-6 bg-white rounded-full"></span>{' '}
                                            <span>EPC Contractor : PECC2</span>
                                        </li>
                                        <li className="flex gap-x-4">
                                            <span className="block w-6 h-6 bg-white rounded-full"></span>{' '}
                                            <span>EPC Contractor : PECC2</span>
                                        </li>
                                    </ul>
                                    <button onClick={handlePopupClose}>
                                        Close popup
                                    </button>
                                </div>
                            </div>
                        </Popup>

                        <Tooltip className="text-blue-500 bg-red-500">
                            {idMarker === mar.id && isMarkerClicked ? (
                                ''
                            ) : (
                                <div className="border-t -translate-x-[105%] relative border-white border-dashed w-[300px]">
                                    <h4 className="absolute top-0 right-0 w-[300px] text-2xl text-white -translate-y-full text-start">
                                        Ca Mau Project
                                    </h4>
                                </div>
                            )}
                        </Tooltip>
                    </Marker>
                ))}
                <ResetCenterView selectPosition={selectPosition} />

                {/* <LayersControl position="topright">
                    <LayersControl.Overlay name="Feature group">
                        <FeatureGroup pathOptions={{ color: 'purple' }}>
                            <Popup>Popup in FeatureGroup</Popup>
                            <Circle
                                center={[107.0454873134701, 16.31811203785642]}
                                radius={200}
                            />
                            <Rectangle bounds={rectangle} />
                        </FeatureGroup>
                    </LayersControl.Overlay>
                </LayersControl> */}
            </MapContainer>
        </>
    );
}
