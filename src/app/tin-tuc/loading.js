import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

export default function LoadingPage() {
    return (
        <>
            <SkeletonTheme baseColor="#c8e7cc" highlightColor="#4CA757">
                <Skeleton count={5} />
            </SkeletonTheme>
        </>
    );
}
