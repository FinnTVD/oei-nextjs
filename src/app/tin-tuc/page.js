'use client';
import { useState } from 'react';
import {
    GET_CATEGORIES,
    GET_POST_FOLLOW_CATEGORIES,
} from '@/graphql/news/queries';

import { useQuery } from '@apollo/client';
import { useRouter } from 'next/navigation';
import LoadingPage from './loading';

const size = 3;

export default function Blog() {
    const [categoryIn, setCategoryIn] = useState('dGVybTox');
    const [offset, setOffset] = useState(0);
    const router = useRouter();

    const {
        loading: loadingPost,
        error: errorPost,
        data: dataPost,
        refetch: refetchPost,
    } = useQuery(GET_POST_FOLLOW_CATEGORIES, {
        variables: { categoryIn, offset, size },
    });
    console.log('🚀 ~ file: page.js:27 ~ Blog ~ dataPost:', dataPost);

    const {
        loading: loadingCategory,
        error: errorCategory,
        data: dataCategory,
    } = useQuery(GET_CATEGORIES);

    if (loadingCategory || loadingPost) return <LoadingPage />;
    if (errorCategory || errorPost)
        return <p>Error: {errorCategory?.message || errorPost?.message}</p>;

    const totalPages = Math.ceil(
        dataPost.posts.pageInfo.offsetPagination.total / 3
    );

    return (
        <main className="text-black">
            <div className="flex justify-center gap-x-4">
                {dataCategory.categories.edges.map((item) => (
                    <div
                        key={item.node.id}
                        onClick={() => {
                            setCategoryIn(item.node.id);
                            setOffset(0);
                            refetchPost({
                                categoryIn: item.node.id,
                                offset: 0,
                                size,
                            });
                        }}
                        className={`${
                            categoryIn === item.node.id
                                ? 'bg-blue-500 pointer-events-none'
                                : ''
                        } px-3 py-2 text-white bg-black cursor-pointer`}
                    >
                        {item.node.name}
                    </div>
                ))}
            </div>

            <div className="text-black">
                {dataPost.posts.edges.map((post) => (
                    <div
                        key={post.node.id}
                        onClick={() =>
                            router.push(
                                `/tin-tuc/${post.node.slug}__${post.node.id}__${categoryIn}`
                            )
                        }
                    >
                        <h2>{post.node.title}</h2>
                    </div>
                ))}

                <div className="flex justify-center gap-x-4">
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            className={`${
                                offset / 3 == index
                                    ? 'bg-blue-500 pointer-events-none'
                                    : ''
                            } p-4 text-white bg-black rounded-md cursor-pointer`}
                            key={index}
                            onClick={() => {
                                setOffset(index * 3);
                                refetchPost({
                                    categoryIn,
                                    offset,
                                    size,
                                });
                            }}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            </div>
        </main>
    );
}
