'use client';

import { useQuery } from '@apollo/client';
import { usePathname, useRouter } from 'next/navigation';
import { GET_POST_RELATE } from '@/graphql/news/queries';
import LoadingPage from '../loading';

const limit = 5;

export default function PageDetail() {
    const pathname = usePathname();
    const b = pathname.split('/');
    const [, id, categoryId] = b[b.length - 1].split('__');
    const router = useRouter();

    const {
        loading: loadingRelate,
        error: errorRelate,
        data: dataRelate,
    } = useQuery(GET_POST_RELATE, {
        variables: {
            postId: id,
            categoryIn: categoryId,
            limit,
        },
    });

    if (loadingRelate) return <LoadingPage />;
    if (errorRelate)
        return <div className="text-black">{errorRelate?.message}</div>;
    const { post } = dataRelate;
    const listPostRelate = dataRelate.relatedPosts.edges;
    return (
        <>
            <div>
                <h1 className="text-black">{post.title}</h1>
                <div
                    className="text-black"
                    dangerouslySetInnerHTML={{ __html: post.content }}
                ></div>
                <span className="text-black">{post.date}</span>
            </div>
            <div>
                <h1 className="text-center text-black">5 post lien quan</h1>
                <ul className="flex justify-center text-black gap-x-4">
                    {listPostRelate.map((item, index) => (
                        <li
                            onClick={() =>
                                router.push(
                                    `/tin-tuc/${item.node.slug}__${item.node.id}__${categoryId}`
                                )
                            }
                            className="p-4 bg-blue-500"
                            key={index}
                        >
                            {item.node.title}
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
}
