import { useEffect } from 'react';
import { useMap, useMapEvent } from 'react-leaflet';

export function ResetCenterView({ selectPosition }) {
    const map = useMap();
    const amap = useMapEvent('click', () => {
        amap.setView([16.31811203785642, 107.0454873134701], amap.getZoom());
    });

    useEffect(() => {
        if (selectPosition) {
            // map.setView(
            //     L.latLng(selectPosition?.lat, selectPosition?.lon),
            //     map.getZoom(),
            //     {
            //         animate: true,
            //     }
            // );
            map.flyTo(selectPosition, map.getZoom());
        }
    }, [selectPosition]);

    return null;
}
