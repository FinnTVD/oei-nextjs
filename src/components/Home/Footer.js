export default function Footer() {
    return (
        <footer className="relative w-full h-auto pt-[calc(6.5625*100vw/100)] ">
            <img
                className="absolute top-0 left-0 object-cover w-full h-full z-[-1]"
                src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Rectangle-9529.png"
                alt=""
            />
            <div className="flex pl-[calc(4.375*100vw/100)] pr-[calc(4.0625*100vw/100)]">
                <div className="flex flex-col items-center w-[calc(18.9375*100vw/100)] mr-[calc(12.3125*100vw/100)]">
                    <img
                        className="w-[calc(11.9375*100vw/100)] h-[calc(7.875*100vw/100)] object-cover"
                        src="https://demo1.okhub.tech/wp-content/uploads/2023/05/image-1703.png"
                        alt=""
                    />
                    <span className="font-bold text-[calc(1.25*100vw/100)] leading-[225%]">
                        Offshore Energy Installation JSC
                    </span>
                </div>
                <ul className="flex flex-col gap-y-[calc(0.8125*100vw/100)] mr-[calc(9.3125*100vw/100)]">
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        About
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        Projects
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        Investor relation
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        Clients & partners
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        Sustainability
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        News
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                        download profile company
                    </li>
                </ul>
                <div className="flex justify-between flex-1">
                    <div className="flex flex-col">
                        <div className="flex flex-col gap-y-[calc(0.5*100vw/100)]">
                            <span className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                                Recruitment
                            </span>
                            <span className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%]">
                                Contact
                            </span>
                        </div>
                        <span className="text-[calc(0.875*100vw/100)] uppercase font-semibold leading-[171%] mt-[calc(8.0625*100vw/100)]">
                            © 2023 OEI Group
                        </span>
                    </div>
                    <div className="w-[calc(17.5*100vw/100)]">
                        <p className="text-[calc(0.875*100vw/100)] uppercase font-bold leading-[171%]">
                            Sign up for email to receive the latest information
                        </p>
                        <p className="mt-[calc(0.5*100vw/100)] mb-[calc(1.5*100vw/100)] text-[calc(0.75100vw/100)] font-normal leading-[150%]">
                            Lorem ipsum dolor sit amet consectetur. Aliquam sed
                            blandit rhoncus id dictum.
                        </p>
                        <form action="" className="flex justify-between">
                            <input
                                className="bg-transparent outline-none placeholder:text-white05"
                                placeholder="Your email"
                                type="email"
                                name=""
                                id="emailUser"
                            />
                            <button>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth="1.5"
                                    stroke="white"
                                    className="w-3 h-3"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25"
                                    />
                                </svg>
                            </button>
                        </form>
                        <div className="border border-solid border-black02 mt-[calc(0.5*100vw/100)] mb-[calc(2.625*100vw/100)]"></div>
                        <ul className="flex gap-x-[calc(1.5*100vw/100)]">
                            <li className="w-[calc(1.25*100VW/100)] h-[calc(1.25*100vw/100)] cursor-pointer">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/fb.png"
                                    alt=""
                                />
                            </li>
                            <li className="w-[calc(1.25*100VW/100)] h-[calc(1.25*100vw/100)] cursor-pointer">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/fb.png"
                                    alt=""
                                />
                            </li>
                            <li className="w-[calc(1.25*100VW/100)] h-[calc(1.25*100vw/100)] cursor-pointer">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/fb.png"
                                    alt=""
                                />
                            </li>
                            <li className="w-[calc(1.25*100VW/100)] h-[calc(1.25*100vw/100)] cursor-pointer">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/fb.png"
                                    alt=""
                                />
                            </li>
                            <li className="w-[calc(1.25*100VW/100)] h-[calc(1.25*100vw/100)] cursor-pointer">
                                <img
                                    className="object-cover w-full h-full"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/fb.png"
                                    alt=""
                                />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="border border-solid border-black03 mt-[calc(2.5*100vw/100)]"></div>
            <div className="flex pl-[calc(4.5625*100vw/100)] items-center">
                <span className="my-[calc(2.125*100vw/100)] inline-block mr-[calc(12.3125*100vw/100)] w-[calc(18.9375*100vw/100)] uppercase text-[calc(0.875*100vw/100)] font-normal leading-[171%]">
                    Address
                </span>
                <p className="text-[calc(0.875*100vw/100)] w-[calc(23.875*100vw/100)] font-normal leading-[171%]">
                    2nd floor, No.44, 30/4 Rd., Ward 9, Vung Tau City, Vietnam
                </p>
            </div>
            <div className="border border-solid border-black03"></div>
            <div className="flex pl-[calc(4.5625*100vw/100)] pt-[calc(1.375*100vw/100)]">
                <span className="uppercase inline-block mr-[calc(12.3125*100vw/100)] w-[calc(18.9375*100vw/100)] text-[calc(0.875*100vw/100)] font-normal leading-[171%]">
                    Contact
                </span>
                <ul className="flex flex-col gap-y-[calc(0.6875*100vw/100)] mb-[calc(3.75*100vw/100)]">
                    <li className="text-[calc(0.875*100vw/100)] font-normal leading-[171%]">
                        (+84) 254 6295268
                    </li>
                    <li className="text-[calc(0.875*100vw/100)] font-normal leading-[171%]">
                        info@oei.com.vn
                    </li>
                </ul>
            </div>
        </footer>
    );
}
