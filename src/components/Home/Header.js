import SlideBanner from './SlideBanner';

export default function Header() {
    return (
        <header className="w-full h-[100vh] relative">
            {/* <img
				className="absolute top-0 left-0 object-cover w-full h-full"
				src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Rectangle-4752.png"
				alt=""
			/> */}
            <div className="absolute top-0 left-0 w-full h-full">
                <SlideBanner />
            </div>
            <div className="flex justify-between py-[calc(0.75*100vw/100)] px-[calc(4.375*100vw/100)] relative z-10">
                <div className="w-[calc(7.625*100vw/100)] h-[calc(4.625*100vw/100)]">
                    <img
                        className="object-cover w-full h-full"
                        src="https://demo1.okhub.tech/wp-content/uploads/2023/05/logo.png"
                        alt=""
                    />
                </div>
                <div className="flex gap-x-[calc(1.625*100vw/100)] items-center">
                    <span className="font-semibold text-[calc(0.875*100vw/100)] leading-[143%]">
                        +7 495 589 12 00
                    </span>
                    <div className="flex justify-center items-center border w-[calc(9.5*100vw/100)] h-[calc(2.5*100vw/100)] rounded-[calc(1.25*100vw/100)] border-solid border-white025">
                        <span className="capitalize font-semibold text-[calc(0.875*100vw/100)] leading-[179%]">
                            Viet Nam
                        </span>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="1.5"
                            stroke="currentColor"
                            className="w-6 h-6"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                            />
                        </svg>
                    </div>
                </div>
            </div>
        </header>
    );
}
