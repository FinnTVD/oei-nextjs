'use client';

import { gsap } from 'gsap';
import Link from 'next/link';
import { useLayoutEffect, useRef, useState } from 'react';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

const arrAbout = new Array(4).fill(0);

export default function About() {
    const [isView, setIsView] = useState(false);
    const parentRef = useRef(null);

    useLayoutEffect(() => {
        let ctx = gsap.context((self) => {
            const boxes = self.selector('.item-about');
            boxes.forEach((box) => {
                gsap.to(box, {
                    x: '0',
                    scrollTrigger: {
                        trigger: box,
                        start: 'top bottom',
                        end: 'top 10%',
                        scrub: true,
                    },
                });
            });
        }, parentRef);

        return () => ctx.revert();
    }, []);

    return (
        <>
            <section className="relative w-full container-about">
                <div ref={parentRef} id="about">
                    <div className="relative item-about1">
                        <span className="text-[calc(1.125*100vw/100)] uppercase flex h-[calc(4.8125*100vw/100)] items-center text-subtileNew leading-[150%] font-bold tracking-[0.12em]">
                            About us
                        </span>
                        <p className="description mb-[calc(3.125*100vw/100)] text-blackAbout text-[calc(2.875*100vw/100)] -tracking-[0.03em] leading-[122%] font-bold">
                            Offshore Energy Installation .,JSC is office based
                            in Vung Tau City, Viet Nam. One of the main logistic
                            hubs for Oil & Gas, Wind power plant.
                        </p>
                        <Link
                            href="/"
                            className="bg-greenPrimary flex justify-center items-center w-[calc(8.9375*100vw/100)] h-[calc(8.9375*100vw/100)] rounded-full font-bold text-[calc(1.125*100vw/100)] leading-[200%]"
                        >
                            See more
                        </Link>
                        <div className="absolute w-[80vw] z-[-1] bottom-0 left-0 -mb-[calc(2*100vw/100)] text-gray02 text-[calc(9.375*100vw/100)] opacity-[0.6] tracking-[2px] uppercase font-bold leading-[125%]">
                            about us
                        </div>
                    </div>
                    {arrAbout.map((item, index) => (
                        <div
                            key={index}
                            className={`item-about${index + 2} item-about`}
                        >
                            <img
                                className="object-cover w-full h-full rounded-[calc(1.5*100vw/100)]"
                                src="https://demo1.okhub.tech/wp-content/uploads/2023/05/phai-1-1.png"
                                alt=""
                            />
                        </div>
                    ))}
                </div>
            </section>
        </>
    );
}
