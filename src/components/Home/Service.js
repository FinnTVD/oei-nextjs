import Link from 'next/link';

import { useRouter } from 'next/navigation';

const arrItem = new Array(5).fill(0);

export default function Service() {
    const router = useRouter();
    return (
        <section
            id="service"
            className="relative flex w-full justify-between h-auto px-[calc(4.375*100vw/100)] pt-[calc(12.5*100vw/100)] pb-[calc(10.375*100vw/100)]"
        >
            <img
                className="back-service absolute z-[-1] top-0 left-0 w-full h-full object-cover"
                src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Rectangle-4753.jpg"
                alt=""
            />
            <h2 className="absolute top-[calc(4.4375*100vw/100)] opacity-[0.2]  right-[calc(4.6875*100vw/100)] font-black text-gray02 text-[calc(11.25*100vw/100)] leading-[104%] uppercase">
                OUR service
            </h2>
            <div className="w-fit">
                <span className="uppercase text-[calc(1.125*100vw/100)] font-bold leading-[150%] text-white02 tracking-[0.12em]">
                    We provide service
                </span>
                <h3 className="pt-[calc(0.5*100vw/100)] pb-[calc(2*100vw/100)] uppercase font-black text-[calc(3.75*100vw/100)] leading-[133%] text-greenPrimary">
                    OUR service
                </h3>
                <p className="font-normal text-[calc(1.625*100vw/100)] leading-[146%] w-[calc(22.875*100vw/100)]">
                    OFFSHORE ENERGY INSTALLATION JSC (OEI) was awarded the order
                    to repair 120kV submarine cable for a windfarm project in Ca
                    Mau province, Vietnam.{' '}
                </p>
            </div>
            <div className="list-service flex flex-col flex-wrap gap-y-[calc(1*100vw/100)] gap-x-[calc(1.875*100vw/100)] w-[calc(29.1875*2*100vw/100+(1.875*100vw/100))] justify-center h-[calc(30.5*3*100vw/100+(2*100vw/100))]">
                {arrItem.map((item, index) => (
                    <div
                        onClick={() => router.push('/')}
                        key={index}
                        className="w-[calc(29.1875*100vw/100)] cursor-pointer overflow-hidden item-service h-[calc(30.5*100vw/100)] px-[calc(1.625*100vw/100)] pt-[calc(2.5*100vw/100)] pb-[calc(2*100vw/100)] flex items-end rounded-[calc(2.1875*100vw/100)] relative before::content-none before:absolute before:top-0 before:left-0 before:h-full before:w-full before:z-30 before:rounded-[calc(2.1875*100vw/100)] hover:before:bg-hoverService before:bg-service"
                    >
                        <img
                            className="absolute top-0 left-0 object-cover w-full h-full rounded-[calc(2.1875*100vw/100)] z-[-1]"
                            src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Group-427321319-1.png"
                            alt=""
                        />
                        <p className="uppercase z-50 font-bold text-[calc(1.5*100vw/100)] leading-[150%]">
                            Removal and Decommissioning Services for Wind Farm
                            and Oil and Gas facilities.
                        </p>
                        <div className="link-item-service z-50 w-[calc(5*100vw/100)] h-[calc(5*100vw/100)] rounded-full flex justify-center items-center bg-linkService absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 backdrop-blur-[27.5px]">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth="1.5"
                                stroke="white"
                                className="w-[calc(1.75*100vw/100)] h-[calc(1.75*100vw/100)]"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M12 4.5v15m7.5-7.5h-15"
                                />
                            </svg>
                        </div>
                        <Link
                            href=""
                            className="absolute top-0 -translate-y-full hover-service left-0 z-50 px-[calc(1.625*100vw/100)] pt-[calc(2.5*100vw/100)]"
                        >
                            <p className="font-normal text-[calc(1.25*100vw/100)] leading-[160%]">
                                OFFSHORE ENERGY INSTALLATION JSC (OEI) was
                                awarded the order to repair 120kV submarine
                                cable for.
                            </p>
                            <div className="mt-[calc(1*100vw/100)] flex items-center gap-x-[calc(0.625*100vw/100)]">
                                <div className="bg-white rounded-full flex justify-center items-center w-[calc(1.5*100vw/100)] h-[calc(1.5*100vw/100)]">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="5"
                                        stroke="#434447"
                                        className="w-[calc(0.875*100vw/100)] h-[calc(0.875*100vw/100)]"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M8.25 4.5l7.5 7.5-7.5 7.5"
                                        />
                                    </svg>
                                </div>
                                <span className="text-[calc(1.25*100vw/100)] leading-[180%] font-bold">
                                    See Detail
                                </span>
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
            <Link
                href="/"
                className="absolute font-normal text-[calc(1.125*100vw/100)] leading-[150%] tracking-[0.44px] right-[calc(13.375*100vw/100)] bottom-[calc(5*100vw/100)] bg-greenPrimary w-[calc(11.25*100vw/100)] h-[calc(11.25*100vw/100)] rounded-full flex justify-center items-center"
            >
                See all
            </Link>
        </section>
    );
}
