import { useState } from 'react';
import SlideMember from './SlideMember';
import CountUp from 'react-countup';
import ScrollTrigger from 'react-scroll-trigger';

const arrMember = new Array(4).fill(0);

export default function Member() {
    const [counterOn, setCounterOn] = useState(false);
    return (
        <section className="bg-member relative h-auto pb-[calc(6.25*100vw/100)] w-full pt-[calc(6.875*100vw/100)] px-[calc(4.375*100vw/100)]">
            <img
                className="absolute top-0 left-0 z-0 object-cover w-full h-full"
                src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Frame.png"
                alt=""
            />
            <SlideMember />
            <ScrollTrigger
                onEnter={() => setCounterOn(true)}
                onExit={() => setCounterOn(false)}
            >
                <p className="relative z-[10] font-extrabold w-[calc(59.125*100vw/100)] text-[calc(2.5*100vw/100)] leading-[116%] mt-[calc(6.25*100vw/100)] mb-[calc(2.5*100vw/100)]">
                    “Company value reflects its ability to generate future
                    profits, management, and market position."
                </p>
                <div className="flex justify-between">
                    {arrMember.map((item, index) => (
                        <div key={index}>
                            <div className="flex gap-x-[calc(0.25*100vw/100)]">
                                <span className="numberMember text-[calc(7.8125*100vw/100)] uppercase font-bold leading-[76%] bg-numberMember">
                                    {counterOn && (
                                        <CountUp
                                            start={0}
                                            end={20}
                                            duration={2}
                                            delay={0}
                                        />
                                    )}
                                </span>
                                <span className="plusMember -mt-[calc(1.625*100vw/100)] uppercase text-[calc(4.6875*100vw/100)] leading-[127%] font-bold bg-plusMember">
                                    +
                                </span>
                            </div>
                            <span className="relative z-[10] mt-[calc(1.5*100vw/100)] block text-[calc(1.5*100vw/100)] leading-[112%] font-bold">
                                Founded year
                            </span>
                        </div>
                    ))}
                </div>
            </ScrollTrigger>
        </section>
    );
}
