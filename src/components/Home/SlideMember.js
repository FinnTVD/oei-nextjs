'use client';
import { useEffect, useRef, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode, Navigation, Thumbs } from 'swiper';
const arrSlide = new Array(3).fill(0);

let spaceBetweenSlides = 0;
typeof window !== 'undefined' && (4.375 * window.screen.width) / 100;

export default function SlideMember() {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    const swiperRef = useRef();

    const [activeIndex, setActiveIndex] = useState(0);
    useEffect(() => {
        const handleResize = () => {
            spaceBetweenSlides = (4.375 * window.screen.width) / 100;
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);
    if (typeof window === 'undefined') return;

    if (typeof window !== 'undefined') {
        spaceBetweenSlides = (4.375 * window.screen.width) / 100;
    }

    const handleSlideClick = (index) => {
        setActiveIndex(index);
    };

    const handleNextSlide = () => {
        swiperRef.current?.slidePrev();
    };

    const handlePrevSlide = () => {
        swiperRef.current?.slideNext();
    };

    return (
        <>
            <div className="flex justify-between w-full">
                <Swiper
                    onSwiper={setThumbsSwiper}
                    direction="vertical"
                    loop={true}
                    slidesPerView={1}
                    draggable={false}
                    scrollbar={false}
                    freeMode={true}
                    watchSlidesProgress={true}
                    modules={[FreeMode, Navigation, Thumbs]}
                    className="h-[calc(14.125*100vw/100)] w-[calc(35.25*100vw/100)] select-none !m-0 pointer-events-none"
                >
                    {arrSlide.map((item, index) => (
                        <SwiperSlide
                            onClick={() => handleSlideClick(index)}
                            key={index}
                        >
                            <span className="font-bold leading-[150%] tracking-[0.12em] uppercase text-[calc(1.125*100vw/100)]">
                                we are
                            </span>
                            <h2 className="text-[calc(3.75*100vw/100)] font-extrabold leading-[133%] mb-[calc(1*100vw/100)]">
                                A member of
                            </h2>
                            <p className="text-[calc(1.5*100vw/100)] leading-[138%] font-normal">
                                A business development organisation helping to
                                make valuable connections between businesses in
                                the global energy sector.
                            </p>
                        </SwiperSlide>
                    ))}
                </Swiper>
                <Swiper
                    id="logoMember"
                    loop={true}
                    spaceBetween={spaceBetweenSlides}
                    slidesPerView={3}
                    centeredSlides={true}
                    // onSlideChange={(swiper) => {
                    //     console.log(swiper);
                    //     setActiveIndex(swiper.activeIndex);
                    // }}
                    thumbs={{ swiper: thumbsSwiper }}
                    modules={[FreeMode, Navigation, Thumbs]}
                    onBeforeInit={(swiper) => {
                        swiperRef.current = swiper;
                    }}
                    initialSlide={activeIndex}
                    className="h-auto w-[calc(46.5*100vw/100)] !m-0"
                >
                    {arrSlide.map((item, index) => (
                        <SwiperSlide
                            onClick={() => handleSlideClick(index)}
                            key={index}
                            className="!w-[calc(8.25*100vw/100)] select-none flex justify-center items-center !h-[calc(8.25*100vw/100)]"
                        >
                            <img
                                className="object-cover w-full h-full"
                                src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Group-427322908.png"
                            />
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
            <div className="flex gap-x-[calc(1*100vw/100)] relative z-[10]">
                <button
                    className="w-[calc(3.75*100vw/100)] select-none btn-slide-member bg-black01 h-[calc(3.75*100vw/100)] rounded-full flex justify-center items-center"
                    onClick={handlePrevSlide}
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="white"
                        className="w-6 h-6"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M15.75 19.5L8.25 12l7.5-7.5"
                        />
                    </svg>
                </button>
                <button
                    className="w-[calc(3.75*100vw/100)] select-none btn-slide-member bg-black01 h-[calc(3.75*100vw/100)] rounded-full flex justify-center items-center"
                    onClick={handleNextSlide}
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="white"
                        className="w-6 h-6"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M8.25 4.5l7.5 7.5-7.5 7.5"
                        />
                    </svg>
                </button>
            </div>
        </>
    );
}
