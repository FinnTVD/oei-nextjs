import Link from 'next/link';
import SlideCustomer from './SlideCustomer';
import SlidePartner from './SlidePartner';

export default function Partner() {
    return (
        <section className="flex w-full">
            <div className="w-[calc(33.25*100vw/100)] bg-partner relative h-auto pt-[calc(10.625*100vw/100)] pl-[calc(4.375*100vw/100)] pr-[calc(4.6875*100vw/100)]">
                <img
                    className="absolute top-0 left-0 object-cover w-full h-full z-[-1] brightness-[130%]"
                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Rectangle-9541.png"
                    alt=""
                />
                <div className="flex flex-col gap-y-[calc(1.5*100vw/100)]">
                    <p className="font-bold text-[calc(3.125*100vw/100)] leading-[116%] capitalize">
                        “Our goal is to establish enduring partnerships to
                        foster mutual development “
                    </p>
                    <Link
                        className="flex shadow-partner justify-center items-center w-[calc(9.375*100vw/100)] h-[calc(9.375*100vw/100)] rounded-full bg-greenPrimary text-[calc(1.125*100vw/100)] font-bold leading-[150%]"
                        href="/"
                    >
                        Contact us
                    </Link>
                </div>
            </div>
            <div className="flex-1 !overflow-hidden">
                <div className="flex flex-col bg-white pl-[calc(2.5*100vw/100)] ">
                    <h3 className="customers-partner w-[calc(41.5*100vw/100)] mt-[calc(4.5*100vw/100)] bg-member text-[calc(2.1875*100vw/100)] font-extrabold capitalize leading-[120%]">
                        “Our customers come from various industries, and we
                        focus on meeting their needs and demands."
                    </h3>
                    <div className="w-full !overflow-hidden pt-[calc(4.5*100vw/100)] pb-[calc(5*100vw/100)]">
                        <SlideCustomer />
                    </div>
                </div>
                <div className="pl-[calc(2.5*100vw/100)] pt-[calc(3*100vw/100)] relative">
                    <img
                        className="absolute top-0 left-0 h-full w-full object-cover -z-[1]"
                        src="https://demo1.okhub.tech/wp-content/uploads/2023/05/Group-427323130.png"
                        alt=""
                    />
                    <div className="flex gap-x-[calc(0.25*100vw/100)] pb-[calc(0.25*100vw/100)]">
                        <span className="font-bold text-[calc(0.875*100vw/100)] leading-[193%] uppercase">
                            customers with us
                        </span>
                        <svg
                            className="-mt-[calc(0.25*100vw/100)]"
                            width="28"
                            height="25"
                            viewBox="0 0 28 25"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <g opacity="0.6">
                                <path
                                    d="M9.12557 11.7359L11.0984 9.66053L0.897801 6.88226C-0.138051 7.05427 0.282352 7.92116 0.622035 8.3331L9.12557 11.7359Z"
                                    fill="#E0E0E0"
                                />
                                <path
                                    d="M13.723 13.2818L10.8828 12.4435L14.6991 21.775C15.3551 22.2169 15.8561 21.641 15.9736 21.3661L13.723 13.2818Z"
                                    fill="#E0E0E0"
                                />
                                <path
                                    d="M12.7279 11.1861L12.0919 8.03879L20.3791 0.35329C21.3568 -0.22263 21.6077 0.602706 21.611 1.08736L12.7279 11.1861Z"
                                    fill="#E0E0E0"
                                />
                            </g>
                        </svg>
                    </div>
                    <h3 className="uppercase font-extrabold text-[calc(2.5*100vw/100)] leading-[120%]">
                        partner
                    </h3>
                    <div className="w-full pt-[calc(2.5*100vw/100)] pb-[calc(6.125*100vw/100)]">
                        <SlidePartner />
                    </div>
                </div>
            </div>
        </section>
    );
}
