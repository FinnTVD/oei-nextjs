'use client';
import { Swiper, SwiperSlide } from 'swiper/react';

const arrSlide = new Array(10).fill(0);

export default function SlidePartner() {
    return (
        <Swiper
            // install Swiper modules
            loop={true}
            spaceBetween={40}
            slidesPerView="auto"
        >
            {arrSlide.map((i, el) => {
                return (
                    <SwiperSlide
                        className="!w-[calc(10.5625*100vw/100)] select-none !h-fit text-black flex flex-col gap-y-[calc(2.8125*100vw/100)]"
                        key={el}
                    >
                        <img
                            className="object-cover w-full h-full"
                            src="https://demo1.okhub.tech/wp-content/uploads/2023/05/333-1.png"
                            alt=""
                        />
                        <img
                            className="object-cover w-full h-full"
                            src="https://demo1.okhub.tech/wp-content/uploads/2023/05/333-1.png"
                            alt=""
                        />
                    </SwiperSlide>
                );
            })}
        </Swiper>
    );
}
