import Link from 'next/link';

const arrNews = new Array(3).fill(0);

export default function News() {
    return (
        <section className="w-full px-[calc(4.375*100vw/100)] py-[calc(7.5*100vw/100)]">
            <div className="flex items-center justify-between mb-[calc(1.875*100vw/100)]">
                <div>
                    <span className="uppercase text-subtileNew font-bold text-[calc(1.125*100vw/100)] leading-[150%]">
                        News
                    </span>
                    <h2 className="uppercase text-[calc(3.75*100vw/100)] font-extrabold text-greenPrimary leading-[133%]">
                        Last news
                    </h2>
                </div>
                <div className="flex gap-x-[calc(1.5*100vw/100)]">
                    <button className="border border-solid outline-none -tracking-[0.4px] font-normal text-[calc(1*100vw/100)] leading-[156%] rounded-[calc(2*100vw/100)] text-blackBtnNews border-blackBtnNews px-[calc(2.75*100vw/100)] py-[calc(1.125*100vw/100)]">
                        In the Media
                    </button>
                    <button className="border border-solid outline-none -tracking-[0.4px] font-normal text-[calc(1*100vw/100)] leading-[156%] rounded-[calc(2*100vw/100)] text-blackBtnNews border-blackBtnNews px-[calc(2.75*100vw/100)] py-[calc(1.125*100vw/100)]">
                        Events
                    </button>
                    <button className="border border-solid outline-none -tracking-[0.4px] font-normal text-[calc(1*100vw/100)] leading-[156%] rounded-[calc(2*100vw/100)] text-blackBtnNews border-blackBtnNews px-[calc(2.75*100vw/100)] py-[calc(1.125*100vw/100)]">
                        In the Media
                    </button>
                </div>
            </div>
            <div>
                {arrNews.map((item, index) => (
                    <div key={index}>
                        <div className="relative flex">
                            <div className="w-[calc(27.4375*100vw/100)] h-[calc(15.9375*100vw/100)]">
                                <img
                                    className="object-cover w-full h-full rounded-[calc(1.125*100vw/100)]"
                                    src="https://demo1.okhub.tech/wp-content/uploads/2023/05/image-1707.png"
                                    alt=""
                                />
                            </div>
                            <div className="w-[calc(20*100vw/100)] ml-[calc(1.875*100vw/100)] mr-[calc(5*100vw/100)]">
                                <p className="text-blackContentNews text-[calc(1.25*100vw/100)] my-[calc(2.5*100vw/100)] font-bold leading-[150%]">
                                    OEI Awarded the Contract for Providing
                                    Submarine Cable Protection System for Binh
                                    Dai Wind Farm Project
                                </p>
                                <div className="flex justify-between">
                                    <span className="uppercase opacity-[0.75] text-grayDate font-normal text-[calc(0.625*100vw/100)] leading-[140%]">
                                        Project news
                                    </span>
                                    <span className="uppercase opacity-[0.75] text-grayDate font-normal text-[calc(0.625*100vw/100)] leading-[140%]">
                                        14 JUL 2021
                                    </span>
                                </div>
                            </div>
                            <p className="w-[calc(23.625*100vw/100)] text-blackContentNews leading-[169%] font-normal text-[calc(1*100vw/100)]">
                                On June 5th 2022, Offshore Energy Installation
                                JSC (OEI) was awarded a Contract of Providing
                                Cable Protection System Installation Service for
                                31 WTGs of Binh Dai Wind Farm Project.
                            </p>
                            <Link
                                href="/"
                                className="absolute bottom-0 right-0 flex gap-x-[calc(0.5*100vw/100)] items-center"
                            >
                                <div className="w-[calc(1.5*100vw/100)] flex justify-center items-center h-[calc(1.5*100vw/100)] bg-greenPrimary rounded-full">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="2"
                                        stroke="white"
                                        className="w-[calc(1*100vw/100)] h-[calc(1*100vw/100)]"
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M8.25 4.5l7.5 7.5-7.5 7.5"
                                        />
                                    </svg>
                                </div>
                                <span className="text-greenPrimary">
                                    See Detail
                                </span>
                            </Link>
                        </div>
                        {index !== arrNews.length - 1 && (
                            <div className="border border-solid border-lineNews my-[calc(1.6875*100vw/100)]"></div>
                        )}
                    </div>
                ))}
            </div>
        </section>
    );
}
