'use client';

import { useState } from 'react';
import { FreeMode, Navigation, Thumbs, Pagination, EffectFade } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

export default function SlideBanner() {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    return (
        <>
            <div className="w-full h-[100vh] relative">
                <Swiper
                    loop={true}
                    effect={'fade'}
                    navigation={false}
                    pagination={{
                        type: 'progressbar',
                    }}
                    thumbs={{ swiper: thumbsSwiper }}
                    modules={[
                        EffectFade,
                        FreeMode,
                        Navigation,
                        Thumbs,
                        Pagination,
                    ]}
                    id="swiper1-banner"
                    className="mySwiper2 absolute top-0 left-0 w-full h-[100vh] -z-[1]"
                >
                    <SwiperSlide className="w-full h-[100vh]">
                        <img
                            className="object-cover w-full h-full"
                            src="https://swiperjs.com/demos/images/nature-1.jpg"
                        />
                    </SwiperSlide>
                    <SwiperSlide className="w-full h-[100vh]">
                        <img
                            className="object-cover w-full h-full"
                            src="https://swiperjs.com/demos/images/nature-2.jpg"
                        />
                    </SwiperSlide>
                    <SwiperSlide className="w-full h-[100vh]">
                        <img
                            className="object-cover w-full h-full"
                            src="https://swiperjs.com/demos/images/nature-3.jpg"
                        />
                    </SwiperSlide>
                </Swiper>
                <Swiper
                    onSwiper={setThumbsSwiper}
                    loop={false}
                    // spaceBetween={100}
                    slidesPerView={3}
                    freeMode={true}
                    watchSlidesProgress={true}
                    modules={[FreeMode, Navigation, Thumbs]}
                    id="swiper2-banner"
                    className="absolute bottom-0 left-0 w-full -translate-y-1/2 mySwiper"
                >
                    <SwiperSlide className="border-solid border-white rounded-full cursor-pointer hover:border-[6px] border-[3px] !w-[calc(6.5*100vw/100)] !h-[calc(6.5*100vw/100)]">
                        <img
                            className="object-cover w-full h-full rounded-full"
                            src="https://swiperjs.com/demos/images/nature-1.jpg"
                        />
                    </SwiperSlide>
                    <SwiperSlide className="border-solid border-white rounded-full cursor-pointer hover:border-[6px] border-[3px] !w-[calc(6.5*100vw/100)] !h-[calc(6.5*100vw/100)]">
                        <img
                            className="object-cover w-full h-full rounded-full"
                            src="https://swiperjs.com/demos/images/nature-2.jpg"
                        />
                    </SwiperSlide>
                    <SwiperSlide className="border-solid border-white rounded-full cursor-pointer hover:border-[6px] border-[3px] !w-[calc(6.5*100vw/100)] !h-[calc(6.5*100vw/100)]">
                        <img
                            className="object-cover w-full h-full rounded-full"
                            src="https://swiperjs.com/demos/images/nature-3.jpg"
                        />
                    </SwiperSlide>
                </Swiper>
            </div>
        </>
    );
}
