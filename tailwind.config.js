/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    ],
    theme: {
        extend: {
            backgroundImage: {
                member: 'linear-gradient(180deg, #4CA757 0%, #16A571 100%)',
                sitemap:
                    'linear-gradient(0deg, rgba(22, 67, 67, 0.61), rgba(22, 67, 67, 0.61))',
                partner:
                    'linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2))',
                numberMember:
                    'linear-gradient(181.38deg, #FFFFFF 1.18%, rgba(255, 255, 255, 0) 117.87%)',
                plusMember:
                    'linear-gradient(180deg, #FFFFFF 30.85%, rgba(255, 255, 255, 0) 81.05%)',
                service:
                    'linear-gradient(180deg, rgba(22, 67, 67, 0) 0%, #164343 100%)',
                hoverService:
                    'linear-gradient(0deg, rgba(22, 67, 67, 0.78), rgba(22, 67, 67, 0.78))',
                marker: 'linear-gradient(180deg, #4CA757 0%, rgba(76, 167, 87, 0) 108.33%)',
            },
            boxShadow: {
                partner: '0px 0px 50px rgba(73, 178, 93, 0.25)',
            },
            colors: {
                gray02: 'rgba(217, 217, 217, 0.2)',
                white02: 'rgba(255, 255, 255, 0.7)',
                white025: 'rgba(255, 255, 255, 0.25)',
                greenPrimary: '#4CA757',
                blackAbout: '#4E4E4E',
                greenSecond: '#164343',
                black06: 'rgba(0, 0, 0, 0.55)',
                black02: 'rgba(255, 255, 255, 0.2)',
                black03: 'rgba(255, 255, 255, 0.3)',
                black01: 'rgba(255, 255, 255, 0.1)',
                linkService: 'rgba(22, 67, 67, 0.71)',
                subtileNew: '#888888',
                blackBtnNews: '#394854',
                blackContentNews: '#3C3C3C',
                grayDate: '#434447',
                lineNews: '#D9D9D9',
                white05: 'rgba(255, 255, 255, 0.5)',
            },
        },
    },
    plugins: [],
};
